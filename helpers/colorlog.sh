#!/usr/bin/env bash

# @STAGE @MSG @STATUS
# Possible values for @STATUS
# 1 - RED color
# 2 - BOLD
# Colorization
RED='\033[0;31m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
BOLD='\033[1;37m'
NC='\033[0m' # No Color

if [[ -z ${1+x} ]];then TOPIC=""; else TOPIC=${1};fi
if [[ -z ${2+x} ]];then MSG=""; else MSG=${2};fi
if [[ -z ${3+x} ]];then STATUS=""; else STATUS=${3};fi

if [[ ${STATUS} -eq 1 ]];then
  echo -e "[ ${GREEN}$(date +"%Y-%m-%d %H:%M:%S.%N %:z")${NC} ] ${CYAN}${TOPIC}${NC} ${RED}${MSG}${NC}"
elif [[ ${STATUS} -eq 2 ]];then
  echo -e "[ ${GREEN}$(date +"%Y-%m-%d %H:%M:%S.%N %:z")${NC} ] ${CYAN}${TOPIC}${NC} ${BOLD}${MSG}${NC}"
else
  echo -e "[ ${GREEN}$(date +"%Y-%m-%d %H:%M:%S.%N %:z")${NC} ] ${CYAN}${TOPIC}${NC} ${MSG}"
fi
