FROM ubuntu:18.04
ENV TERRAFORM_VERSION=0.12.24
ENV DEBIAN_FRONTEND noninteractive

# Install required packages
RUN apt-get update -y && apt-get install curl unzip apt-utils apt-transport-https gettext-base \
    gnupg ca-certificates ssh mysql-client sshpass jq git python3 python3-pip python3-setuptools \
    -y --no-install-recommends && \
    rm -rf /var/lib/apt/lists/*

# Install Docker and enable an experimental feature for a possibility to retrieve docker images info
RUN curl -fsSL https://get.docker.com -o /tmp/get-docker.sh && \
    sh /tmp/get-docker.sh && \
    mkdir -p /root/.docker && echo '{"experimental": "enabled"}' > /root/.docker/config.json

# Install helpers
COPY /helpers/ /helpers/
RUN install /helpers/colorlog.sh /usr/local/bin/colorlog

# Install Python packages
RUN pip3 install -U pip wheel && \
    pip3 install python-gitlab==2.0.0 pyyaml==5.3 ansible==2.9

# Install AWS CLI 2
# https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install && \
    rm -rf awscliv2.zip ./aws

# Install Google Cloud SDK
RUN echo "deb http://packages.cloud.google.com/apt cloud-sdk-bionic main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && \
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - && \
    apt-get update -y && \
    apt-get install google-cloud-sdk -y --no-install-recommends && \
    rm -rf /var/lib/apt/lists/*

# Install terraform
RUN curl -sSL https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip -o /tmp/terraform.zip && \
    unzip /tmp/terraform.zip -d /usr/bin && \
    rm /tmp/terraform.zip
